import unittest
import platform
from pathlib import Path
from unittest.mock import patch
import tempfile

import pysimi.data as sd
import pysimi.data.tests.project_path_defintions as ppd


def add_key(magic_key):
    return


class TestClassProject(unittest.TestCase):

    def test_Project_init_exceptions(self):
        with self.assertRaises(FileNotFoundError):
            sd.Project("undefined_project_path.blub")
        with patch('pathlib.Path.is_file', return_value=True), self.assertRaises(ValueError):
            sd.Project("undefined_file_type.blub")

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_smp_xmp_converters(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            smp_path = ppd.data_path / Path('datagroups.smp')
            xmp_path_modified = tmpdirname / Path('datagroups_test081542.xmp')
            sd.Project.smp2xmp(smp_path, xmp_path_modified)
            self.assertTrue(xmp_path_modified.is_file())

            xmp_path = ppd.data_path / Path('datagroups.xmp')
            smp_path_modified = tmpdirname / Path('datagroups_test081542.smp')
            sd.Project.xmp2smp(xmp_path, smp_path_modified)
            self.assertTrue(smp_path_modified.is_file())

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_SmProject_initialisation(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        project = sd.Project(sm_path)
        self.assertIsInstance(project.internal_project, sd.SmProject)

    def test_XmProject_initialisation(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        project = sd.Project(xm_path)
        self.assertIsInstance(project.internal_project, sd.XmProject)

    def test_get_videos(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        project = sd.Project(xm_path)
        videos_found = project.get_videos(path_update=False)
        for vid in ppd.datagroups_video_files:
            self.assertTrue(vid in videos_found, msg='file not found: ' + str(vid))

    def test_search_file_recursively(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            start_path = Path('more/path/parts/Projects/0815_Calibration/Data/cam42_new.avi')
            path_to_find = tmpdirname / Path('0815_Calibration/Data/cam42_new.avi')
            path_to_find.parent.mkdir(parents=True, exist_ok=True)
            path_to_find.touch()
            corrected_path = sd.Project.search_file_recursively(tmpdirname, start_path)
            self.assertEqual(path_to_find, corrected_path)

    def test_find_data(self):
        sd.classes.magickeys.add_unknown = add_key  # necessary to deal with unknown magic key CIDY in xmp file
        xm_path = ppd.data_path / Path('datagroups_multi_tracks.xmp')
        project = sd.Project(xm_path)
        xm_data = project.find_data_row(data_group_id=200613, data_row_id=7670)
        self.assertEqual(7670, xm_data.id)

    def test_get_empty_linked_data_row(self):
        xm_path = ppd.data_path / Path('data_links.xmp')
        xm_project = sd.Project(xm_path)

        group = None

        for g in xm_project.walk_data_group():
            if g.id == 200004:
                group = g

        self.assertIsNotNone(group)

        # Data Row with name "Root"
        row = group.get_data_row_by_id(0, 9000)

        self.assertIsNotNone(row)
        self.assertEqual("Root", row.name)
        self.assertEqual("Linked Data Row", row.type_name)
        self.assertEqual("Linked", row.short_type_name)


if __name__ == '__main__':
    unittest.main()
