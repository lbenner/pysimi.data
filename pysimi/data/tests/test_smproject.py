import unittest
import numpy as np
import platform
from pathlib import Path

import pysimi.data as sd
import pysimi.data.tests.project_path_defintions as ppd


class TestClassSmProject(unittest.TestCase):
    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_init(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        self.sm_project = sd.SmProject(sm_path)
        self.assertEqual(sm_path, self.sm_project.path)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_videos(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        sm_project = sd.SmProject(sm_path)
        videos_found = sm_project.get_videos()
        for vid in ppd.datagroups_video_files:
            self.assertTrue(vid in videos_found, msg='file not found: ' + str(vid))

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_data_group_with_none(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        sm_project = sd.SmProject(sm_path)

        data_group_container = sm_project.get_data_group(None)

        data_group_ids = sorted([ds.id for ds in data_group_container])

        self.assertEqual([200070, 200073, 200076, 200079, 200082, 200085, 200088, 200091, 200094, 200095, 200096,
                          200097, 200098, 200099, 200100, 200101, 200103], data_group_ids)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_data_group_with_magick(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        sm_project = sd.SmProject(sm_path)

        data_group_container = sm_project.get_data_group(sd.MagicKeys.FPLC)

        data_group_ids = sorted([ds.id for ds in data_group_container])

        self.assertEqual([200098, 200099, 200100, 200101], data_group_ids)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_walk_data_group(self):
        sm_path = ppd.data_path / Path('datagroups.smp')
        sm_project = sd.SmProject(sm_path)
        counter = 0
        for _ in sm_project.walk_data_group():
            counter += 1
        self.assertEqual(17, counter)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_data_row_by_index(self):
        sm_path = ppd.data_path / Path('datagroups_multi_tracks.smp')
        sm_project = sd.SmProject(sm_path)
        impl_data = sm_project.project.Datas(200609)
        data_row = sm_project.get_data_row_by_index(impl_data, 0, 0)
        self.assertEqual("Pelvis", data_row.name)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_data_row_by_id(self):
        sm_path = ppd.data_path / Path('datagroups_multi_tracks.smp')
        sm_project = sd.SmProject(sm_path)
        impl_data = sm_project.project.Datas(200609)
        data_row = sm_project.get_data_row_by_id(impl_data, 0, 9007)
        self.assertEqual("Pelvis", data_row.name)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_data_row(self):
        sm_path = ppd.data_path / Path('datagroups_multi_tracks.smp')
        sm_project = sd.SmProject(sm_path)
        data_group = sm_project.project.Datas(200609)
        impl_data = data_group.Data(0, 9007)
        row = sm_project.get_data_row(impl_data)
        self.assertIsInstance(row, np.ndarray)
        self.assertEqual((1005,), row.shape)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_all_data_rows(self):
        sm_path = ppd.data_path / Path('datagroups_multi_tracks.smp')
        sm_project = sd.SmProject(sm_path)
        data_group = sm_project.project.Datas(200609)
        row = sm_project.get_all_data_rows(data_group, 9007)
        self.assertIsInstance(row, np.ndarray)
        self.assertEqual((1005, 3), row.shape)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_get_phase_group_with_none(self):
        sm_path = ppd.data_path / Path('Phase.smp')
        sm_project = sd.SmProject(sm_path)
        phase_group_container = sm_project.get_phase_group()
        self.assertEqual(phase_group_container[0].name, "Phases")

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_walk_phase_row(self):
        sm_path = ppd.data_path / Path('Phase.smp')
        sm_project = sd.SmProject(sm_path)
        counter = 0
        for _ in sm_project.walk_phase_group():
            counter += 1
        self.assertEqual(1, counter)

    @unittest.skipIf(platform.system() is not 'Windows', 'non Windows OS')
    def test_phase_row_by_index(self):
        sm_path = ppd.data_path / Path('Phase.smp')
        sm_project = sd.SmProject(sm_path)
        impl_data = sm_project.project.PhasesByIndex(0)
        phase_row = sm_project.get_phase_row_by_index(impl_data, 0)
        self.assertEqual("Start", phase_row.name)


if __name__ == '__main__':
    unittest.main()
