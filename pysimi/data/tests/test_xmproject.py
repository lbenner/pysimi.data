import unittest
from pathlib import Path
import numpy as np

import pysimi.data as sd
import pysimi.data.tests.project_path_defintions as ppd


class TestClassXmProject(unittest.TestCase):
    def test_init(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        xm_project = sd.XmProject(xm_path)
        self.assertEqual(xm_path, xm_project.path)

    def test_get_videos(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        xm_project = sd.XmProject(xm_path)
        videos_found = xm_project.get_videos()
        for vid in ppd.datagroups_video_files:
            self.assertTrue(vid in videos_found, msg='file not found: ' + str(vid))

    def test_get_data_group_with_none(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        xm_project = sd.XmProject(xm_path)

        data_group_container = xm_project.get_data_group(None)

        data_group_ids = sorted([ds.id for ds in data_group_container])

        self.assertEqual([200070, 200073, 200076, 200079, 200082, 200085, 200088, 200091, 200094, 200095, 200096,
                          200097, 200098, 200099, 200100, 200101, 200103], data_group_ids)

    def test_get_data_group_with_magick(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        xm_project = sd.XmProject(xm_path)

        data_group_container = xm_project.get_data_group(sd.MagicKeys.FPLC)

        data_group_ids = sorted([ds.id for ds in data_group_container])
        self.assertEqual([200098, 200099, 200100, 200101], data_group_ids)

    def test_walk_data_group(self):
        xm_path = ppd.data_path / Path('datagroups.xmp')
        xm_project = sd.XmProject(xm_path)
        counter = 0
        for _ in xm_project.walk_data_group():
            counter += 1
        self.assertEqual(17, counter)

    def test_data_row_by_index(self):
        xm_path = ppd.data_path / Path('datagroups_multi_tracks.xmp')
        xm_project = sd.XmProject(xm_path)
        impl_data = xm_project.tree.find("Project/ProjectData/Datas/[ID='200609']")
        data_row = xm_project.get_data_row_by_index(impl_data, 0, 0)
        self.assertEqual("Pelvis", data_row.name)

        with self.assertRaises(ValueError):
            xm_project.get_data_row_by_index(impl_data, 0, 42)  # data_index too large

        with self.assertRaises(ValueError):
            xm_project.get_data_row_by_index(impl_data, 3, 0)  # track_index too large

    def test_data_row_by_id(self):
        xm_path = ppd.data_path / Path('datagroups_multi_tracks.xmp')
        xm_project = sd.XmProject(xm_path)
        impl_data = xm_project.tree.find("Project/ProjectData/Datas/[ID='200609']")
        data_row = xm_project.get_data_row_by_id(impl_data, 0, 9007)
        self.assertEqual("Pelvis", data_row.name)

        with self.assertRaises(ValueError):
            xm_project.get_data_row_by_id(impl_data, 3, 9007)  # track_index too large

        with self.assertRaises(ValueError):
            xm_project.get_data_row_by_id(impl_data, 0, 420815)  # data_id does not exist

    def test_get_data_row(self):
        xm_path = ppd.data_path / Path('datagroups_multi_tracks.xmp')
        xm_project = sd.XmProject(xm_path)
        data_group = xm_project.tree.find("Project/ProjectData/Datas/[ID='200609']")
        impl_data = data_group.find("./Data/ID[.='9007']..")
        row = xm_project.get_data_row(impl_data)
        self.assertIsInstance(row, np.ndarray)
        self.assertEqual((1005,), row.shape)

    def test_get_all_data_rows(self):
        xm_path = ppd.data_path / Path('datagroups_multi_tracks.xmp')
        xm_project = sd.XmProject(xm_path)
        data_group = xm_project.tree.find("Project/ProjectData/Datas/[ID='200609']")
        row = xm_project.get_all_data_rows(data_group, 9007)
        self.assertIsInstance(row, np.ndarray)
        self.assertEqual((1005, 3), row.shape)

    def test_get_phase_group_with_none(self):
        xm_path = ppd.data_path / Path('Phase.xmp')
        xm_project = sd.XmProject(xm_path)
        phase_group_container = xm_project.get_phase_group(0)
        self.assertEqual(phase_group_container[0].name, "Phases")

    def test_walk_phase_group(self):
        xm_path = ppd.data_path / Path('Phase.xmp')
        xm_project = sd.XmProject(xm_path)
        counter = 0
        for _ in xm_project.walk_phase_group():
            counter += 1
        self.assertEqual(1, counter)

    def test_walk_group_with_linked_data_rows(self):
        xm_path = ppd.data_path / Path('data_links.xmp')
        xm_project = sd.XmProject(xm_path)
        group_count = 0
        row_count = 0
        for g in xm_project.walk_data_group():
            group_count += 1
            for _ in g.walk_data_row():
                row_count += 1
        self.assertEqual(31, group_count)
        self.assertEqual(1102, row_count)

    def test_phase_row_by_index(self):
        xm_path = ppd.data_path / Path('Phase.xmp')
        xm_project = sd.XmProject(xm_path)
        impl_data = xm_project.tree.findall("Project/ProjectPhases/Phases")[0]
        phase_row = xm_project.get_phase_row_by_index(impl_data, 0)
        self.assertEqual('Start', phase_row.name)


if __name__ == '__main__':
    unittest.main()
