import platform
from pathlib import Path
from typing import Union
import numpy as np
import shutil

from pysimi.data.classes.data_row import DataRow
from pysimi.data.classes.data_group import DataGroup
from pysimi.data.classes.magickeys import MagicKey
from pysimi.data.classes.magickeys import MagicKeys
from pysimi.data.classes.projectbase import ProjectBase
from pysimi.data.classes.phase_row import PhaseRow
from pysimi.data.classes.phase_group import PhaseGroup

GET_DATA_ROW_AS_FLOAT = 1


def delete_cache_com_directory(com_name: str):
    """
    Delete the cache com directory of an existing com_object

    :param com_name: name of the com object the cache shell be deleted for
    :return:
    """
    import win32com.client

    # Get folder and path name where COM Object folder is created
    disp = win32com.client.Dispatch(com_name)
    ti = disp._oleobj_.GetTypeInfo()
    tlb, index = ti.GetContainingTypeLib()
    tla = tlb.GetLibAttr()
    fold_name = win32com.client.gencache.GetGeneratedFileName(tla[0], tla[1], tla[3], tla[4])
    path = win32com.client.gencache.GetGeneratePath()
    abs_path = Path(path) / Path(fold_name)

    if abs_path.is_dir():
        shutil.rmtree(abs_path)


def load_smlib():
    """
    load the SmLib, only successful if windows OS and valid license exist

    :return: SmLib if successful, otherwise None
    """
    try:
        import win32com.client
        smlib = win32com.client.gencache.EnsureDispatch('SIMIData.SmLib')
    except AttributeError:
        delete_cache_com_directory('SIMIData.SmLib')
        raise SystemError('In irregular intervals the com cache folder gets damaged.'
                          '\n\t\t\tBy now, it got deleted, so please try to rerun your code.')
    except:
        smlib = None
    return smlib


def create_data_group(project, smlib_data_group):
    data_group = DataGroup(project, smlib_data_group,
                  smlib_data_group.ID,
                  MagicKeys.get(smlib_data_group.Magic),
                  smlib_data_group.Name,
                  smlib_data_group.TrackCount,
                  smlib_data_group.Count)
    return data_group


def create_phase_group(project, smlib_phase_group, phase_group_count, count):
    phase_group = PhaseGroup(project, smlib_phase_group, smlib_phase_group.Name, phase_group_count, count)
    return phase_group


def create_data_row(self, smlib_parent, smlib_data_row):
    data_row = DataRow(self, smlib_parent, smlib_data_row, smlib_data_row.Name, smlib_data_row.TypeName,
                       smlib_data_row.ShortTypeName, smlib_data_row.UnitName, smlib_data_row.ID, smlib_data_row.Size,
                       smlib_data_row.First, smlib_data_row.Last)
    return data_row


def create_phase_row(self, smlib_parent, smlib_phase_row):
    phase_row = PhaseRow(self, smlib_parent, smlib_phase_row, smlib_phase_row.Name, smlib_phase_row.Start,
                         smlib_phase_row.End)
    return phase_row


class DatasIterator:
    def __init__(self, project):
        self.project = project

    def __iter__(self):
        self.data_row_count = self.project.project.NumberOfDatas
        self.current_data_row = 0
        return self

    def __next__(self):
        if self.current_data_row < self.data_row_count:
            smlib_data_group = self.project.project.DatasByIndex(self.current_data_row)
            self.current_data_row += 1
        else:
            raise StopIteration()
        return create_data_group(self.project, smlib_data_group)


class PhaseIterator:
    def __init__(self, project):
        self.project = project

    def __iter__(self):
        self.phase_row_count = self.project.project.NumberOfPhases
        self.current_phase_row = 0
        return self

    def __next__(self):
        if self.current_phase_row < self.phase_row_count:
            smlib_phase_group = self.project.project.PhasesByIndex(self.current_phase_row)
            self.current_phase_row += 1
        else:
            raise StopIteration()
        return create_phase_group(self.project, smlib_phase_group, self.current_phase_row, smlib_phase_group.Count)


class SmProject(ProjectBase):
    """
    Provide several functions to load, read and manipulate smp files.

    This should never be called directly! Use a Project element for obtaining a SmProject element.
    Class can only be used on windows and with a valid license.
    """
    smlib = load_smlib()

    def __init__(self, path: Union[Path, str]):
        super(SmProject, self).__init__(path)
        if platform.system() is not 'Windows':
            raise OSError('The class SmProject is only implemented for windows')
        elif self.smlib is None:
            raise ValueError('No SIMIData.SmLib found. Does a valid license exist?')
        self.project = SmProject.smlib.OpenProject(str(path))

    def get_videos(self):
        """
        Collect all videos from project and return list of paths

        :return: list of all video paths
        """
        avis = set()
        cams_count = self.project.NumberOfCameras
        for cams_idx in range(cams_count):
            cams = self.project.CamerasByIndex(cams_idx)
            for cam_idx in range(cams.Count):
                cam = cams.CameraByIndex(cam_idx)
                avi = cam.AVIFilenameMotion
                if avi:
                    avis.add(avi)
        return [Path(x) for x in avis]

    def get_data_group(self, magic: MagicKey):
        data_group_collections = []
        for n in range(self.project.NumberOfDatas):
            smlib_data_group = self.project.DatasByIndex(n)
            if (magic is None) or (magic == MagicKeys.DATA_GROUP_NULL) or (magic.magic == smlib_data_group.Magic):
                data_group = create_data_group(self, smlib_data_group)
                data_group_collections.append(data_group)
        return data_group_collections

    def walk_data_group(self):
        return DatasIterator(self)

    def get_data_row_by_index(self, impl_data, track_index: int, data_row_index: int):
        smlib_data_row = impl_data.DataByIndex(track_index, data_row_index)
        return create_data_row(self, impl_data, smlib_data_row)

    def get_data_row_by_id(self, impl_data, track_index: int, data_row_id: int):
        smlib_data_row = impl_data.Data(track_index, data_row_id)
        return create_data_row(self, impl_data, smlib_data_row)

    def get_data_row(self, impl_data):
        row = impl_data.Data(GET_DATA_ROW_AS_FLOAT)
        return np.array(row)

    def get_all_data_rows(self, group, data_row_id: int):
        data_rows = []
        for track_index in range(group.TrackCount):
            impl_data = group.Data(track_index, data_row_id)
            data_row = impl_data.Data(GET_DATA_ROW_AS_FLOAT)
            data_rows.append(np.array(data_row))
        return np.stack(data_rows, 0).T

    def get_phase_group(self, index=0):
        phase_group_collections = []
        count = self.project.NumberOfPhases
        if index < count:
            for n in range(count):
                smlib_phase_group = self.project.PhasesByIndex(n)
                phase_group = create_phase_group(self, smlib_phase_group, index, count)
                phase_group_collections.append(phase_group)
        else:
            raise IndexError("This Phase group with following {} index does not exist. "
                             "Number of PhaseGroups available are {}".format(index, count))
        return phase_group_collections

    def walk_phase_group(self):
        return PhaseIterator(self)

    def get_phase_row_by_index(self, impl_data, phase_row_index):
        smlib_phase_row = impl_data.Phase(phase_row_index)
        return create_phase_row(self, impl_data, smlib_phase_row)

